//
// Created by user on 6/9/19.
//

#include <iostream>
#include "threadpool.hpp"
#include "connectionmanager.hpp"

int main(int argc, char* argv[]) {
    // handle program args

    // convert to vector for more sensible handling
    std::vector<std::string> args;
    for (int i = 0; i < argc; ++i) {
        args.push_back(argv[i]);
    }

    for (auto &i : args) {
        std::cout << i << std::endl;
    }

    ThreadPool tp;
    tp.spawnWorkers(2);

    ConnectionManager cm;

    // request processing fn
    auto process_msg = [](ClientCtxPtr, uint8_t tag, DataWrapper data) {
        std::cout << data.getString() << std::endl;
    };
    cm.processMessageSignal.connect(process_msg);


    auto ctx = cm.connectTo("127.0.0.1", 6321);

    for (int i = 1; i < args.size(); ++i)
        ctx->send(args[i]);
    while(true)
        cm.process();
}