# JSON API

## Overview
Interacting with the server requires use of the JSON API. Most messages to the server are actions, used to do things
such as query the server for information or trigger functions. Messages received from the server are either responses to
actions or information updates from the server.

## Format

Action message:

    {
        "message-type" : "action",
        "action" : "<action-type>",
        
        # optional data, usually containing arguments for the action
        "data" : {
            <contents>,
        }
    }
    
Response message:

    {
        "message-type" : "response",
    
        # data is always present in responses
        "data" : {
            <contents>
        }
    }

Update message:

    {
        "message-type" : "update",
        "update" : "<update-type>",
        
        # data is always present in updates
        "data" : {
            <contents>
        }
    }
    
As you can see, all types of messages are formatted similarly at a basic level. Their type is contained in `message-type`,
and they all have an associated `data` section containing any relevant data.

## Actions

### Library actions

#### Directory listing

**Action**

    "action" : "list-dir",
    "data" : {
        "library" : "<library-name>",
        "path" : "/directory/inside/library"
    }
    
`library` : The library you are querying  
`path` : The path relative to the library root. Leading slash always, trailing slash never.

**Response**

    "data" : {
        "library" : "<library-name>",
        "dir" : "<directory>",
        "entries" : [
            {
                "name" : "<filename>",
                "type" : "<type>",
            }
            ...
        ]
    }

`library` : Name of the library being queried
`directory` : Path being queried  
`entries` : Contains a list of objects with info on each filesystem entry  
`filename` : The name of the file including extension. Does not contain the path.  
`type` : `file` or `dir` depending on which it is.  

### Queue actions

*Notes about the queue:*  
- The queue is 0 indexed
- To track changes in the queue you will need to subscribe to it

#### Remove track from queue

    "action" : "queue-remove",
    "data" : {
        "index" : <index>
    }
    
`index` : The index of the track you want to remove
