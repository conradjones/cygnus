# The Cygnus networking API

*Note: This software is early in development. Documentation may not be up to date.*

## Overview

Currently, the only supported method of communicating with the Cygnus server is over TCP sockets. Messages sent to and from
the server are formatted with a header followed by a section of bytes, usually containing json messages in text.
Messages are asynchronous and the server is not guaranteed to respond to requests in any specific order.

## TCP Format

Each message contains a 9 byte header followed by a blob of data.

Bytes:

- 0-7: Data length (unsigned 64 bit)
- 8: Request tag
- 9+: Data

### Request tag

The request tag is a single byte of data used for identifying requests. When the server receives a message, any response
to that message will contain the same tag as the request. For example, if you query the server for a directory listing
using a request tag of 5, the response to that message will contain 5 in the request tag spot. 0 is reserved for
use by standalone messages, such as update broadcasts. In practice, you will want to reuse tags, and it is recommended
you keep track of which tags are in use.

### Sending messages

The only messages you ever send to the server should contain unicode formatted json data. See the json API for more
details. Messages received from the server will only contain unicode formatted json data unless specifically noted.