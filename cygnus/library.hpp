//
// Created by user on 6/14/19.
//

#ifndef CYGNUS_LIBRARY_HPP
#define CYGNUS_LIBRARY_HPP


#include <string>
#include <map>
#include <boost/filesystem.hpp>

#include "clientcontext.hpp"
#include "trackmetadata.hpp"
#include "database.hpp"

class Library {
public:
    Library();
    bool addSource(const std::string& name, const boost::filesystem::path& path);

    json getListingJSON(const std::string &source, const std::string &path);

    std::string getTrackPath(const std::string& library, const std::string& path);
    std::string getTrackPath(const std::string& uri);
    TrackMetadata getTrackMetadata(const std::string &uri);

    /// Find any valid tracks in the provided path
    std::vector<std::string> getTracksInPath(const std::string &source, const std::string &path);

    /// Get all tracks by artists ordered by album year -> track #
    std::vector<std::string> getTracksByArtist(const std::string &name);
    std::vector<std::string> getTracksInAlbum(const std::string &artist, const std::string &album);
    std::vector<std::string> getTracksInAlbum(int album_id);
    std::vector<std::string> getSources();

    /// Get all tracks from al

    int scan(const std::string &source, const std::string &path);
    json search(const std::string &mode, const std::string &query, int limit);
protected:
    void mSetupSchema();

    std::map<std::string, boost::filesystem::path> mSources;
    Database mDB;
};


#endif //CYGNUS_LIBRARY_HPP
