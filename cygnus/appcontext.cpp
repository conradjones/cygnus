//
// Created by user on 12/19/19.
//

#include "appcontext.hpp"

AppContext::AppContext(ThreadPool &pool) :
    mThreadPool(pool),
    mPlayQueue(mSubscriptionManager, mLibrary),
    mAudioProvider(*this, mPlayQueue, mThreadPool, mSubscriptionManager),
    mOutput(mThreadPool, mPlayQueue, mSubscriptionManager, mAudioProvider)
{

}

Library &AppContext::getLibrary() {
    return mLibrary;
}

ThreadPool &AppContext::getThreadPool() {
    return mThreadPool;
}

SubscriptionManager &AppContext::getSubscriptionManager() {
    return mSubscriptionManager;
}

PlayQueue &AppContext::getPlayQueue() {
    return mPlayQueue;
}

AudioProvider &AppContext::getAudioProvider() {
    return mAudioProvider;
}

Output &AppContext::getOutput() {
    return mOutput;
}
