//
// Created by user on 6/8/19.
//

#ifndef CYGNUS_OUTPUT_HPP
#define CYGNUS_OUTPUT_HPP

#include <vector>
#include <string>
#include <queue>
#include <atomic>

#include "miniaudio.h"
#include "threadpool.hpp"
#include "playqueue.hpp"
#include "clientcontext.hpp"
#include "subscriptionmanager.hpp"
#include "audioprovider.hpp"

class Output {
public:
    Output(ThreadPool& pool, PlayQueue &queue, SubscriptionManager &subManager, AudioProvider &provider);

    void queueTrack(const std::string& trackName);
    bool start();
    void stop();
    void togglePause();
    void skip();
    bool formatChange();

    static void dataCallback(ma_device *device, void *output, const void *input, ma_uint32 frameCount );
protected:

    ma_result mResult;
    ma_decoder mDecoder;
    ma_device_config mDeviceConfig;
    ma_device mDevice;

    bool mPaused;
    bool mPlaying;
    unsigned mSampleRate;
    std::atomic<bool> mSkip;
    std::atomic<long> mFramesElapsed;
    std::atomic<long> mLastUpdateFrame;
    std::atomic<long> mTrackLength;
    ThreadPool& mPool;

    //std::queue<std::string> mTrackQueue;
    PlayQueue& mPlayQueue;
    SubscriptionManager &mSubManager;
    AudioProvider &mProvider;
};


#endif //CYGNUS_OUTPUT_HPP
