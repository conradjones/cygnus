//
// Created by user on 12/11/19.
//

#ifndef CYGNUS_AUDIOPROVIDER_HPP
#define CYGNUS_AUDIOPROVIDER_HPP

#include <cstdint>
#include <boost/signals2/signal.hpp>
#include <vector>

#include "playqueue.hpp"
#include "miniaudio.h"
#include "threadpool.hpp"
#include "subscriptionmanager.hpp"

class AppContext;

class AudioProvider {
public:
    AudioProvider(AppContext &app_context, PlayQueue &play_queue, ThreadPool &pool, SubscriptionManager &sub_manager);

    void loadFramesToBuffer(uint32_t frame_count, void *out_buffer);
    bool isFormatSwitchPending() const;
    bool switchFormatSkip();

    bool setCurrentTrack(const std::string &path);
    bool setNextTrack(const std::string &path);

    bool skip();
    bool play();
    bool pause();
    bool stop();
    bool togglePause();

    ma_format getFormat() const;

    uint32_t getChannels() const;

    uint32_t getSampleRate() const;

    boost::signals2::signal<void()> startSignal;
    boost::signals2::signal<void()> stopSignal;
    boost::signals2::signal<void()> formatChangeSignal;
private:
    void mTimeSubscription();

    std::atomic<bool> mPendingFormatSwitch;
    std::atomic<bool> mIsPlaying;
    std::atomic<bool> mIsPaused;
    std::atomic<uint64_t> mCurrentTrackFramesElapsed;
    std::atomic<uint64_t> mCurrentTrackLength;
    uint64_t mUpdateTimer;
    uint32_t mFrameSize;
    std::string mCurrentTrackURI;
    std::string mNextTrackURI;

    ma_decoder mDecoder;
    ma_decoder mNextDecoder;

    ma_format mFormat;
    uint32_t mChannels;
    uint32_t mSampleRate;

    AppContext &mAppContext;
    PlayQueue &mPlayQueue;
    ThreadPool &mThreadPool;
    SubscriptionManager &mSubManager;

    std::shared_ptr<spdlog::logger> logger_;
};


#endif //CYGNUS_AUDIOPROVIDER_HPP
