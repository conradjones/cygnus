//
// Created by user on 12/11/19.
//

#include "audioprovider.hpp"
#include "appcontext.hpp"

#define DR_FLAC_IMPLEMENTATION
#include "extras/dr_flac.h"

#define DR_MP3_IMPLEMENTATION
#include "extras/dr_mp3.h"

#define MINIAUDIO_IMPLEMENTATION
#include "miniaudio.h"

#include <boost/bind/bind.hpp>
#include <iostream>

AudioProvider::AudioProvider(AppContext &app_context, PlayQueue &play_queue, ThreadPool &pool, SubscriptionManager &sub_manager) :
        mAppContext(app_context),
        mPlayQueue(play_queue),
        mThreadPool(pool),
        mSubManager(sub_manager),

        mPendingFormatSwitch(false),
        mIsPlaying(false),
        mIsPaused(false),
        mCurrentTrackFramesElapsed(0),
        mFormat(ma_format_s16),
        mChannels(2),
        mSampleRate(44100),
        mUpdateTimer(0),
        logger_(spdlog::get("cygnus"))
{
    mPlayQueue.trackChangedSignal.connect(boost::bind(&AudioProvider::setCurrentTrack, this, _1));
    //mPlayQueue.stopSignal.connect(boost::bind(&AudioProvider::stop, this));
}

void AudioProvider::loadFramesToBuffer(uint32_t frame_count, void *out_buffer) {
    if (mIsPaused.load() || !mIsPlaying.load()) return;

    ma_decoder_read_pcm_frames(&mDecoder, out_buffer, frame_count);
    mCurrentTrackFramesElapsed.store(mCurrentTrackFramesElapsed.load() + frame_count);
    mUpdateTimer += frame_count;
    if (mUpdateTimer >= mSampleRate) {
        mUpdateTimer %= mSampleRate;
        mThreadPool.queueFn(std::bind(&AudioProvider::mTimeSubscription, this));
    }

    // check if we need to go to the next track
    if ((long)mCurrentTrackLength.load() - (long)mCurrentTrackFramesElapsed.load() <= 0) {
        if (!mPlayQueue.hasNext()) {
            mThreadPool.queueFn(std::bind(&AudioProvider::stop, this));
        } else {
            mPlayQueue.skipRelative(1);
        }
    }
}

bool AudioProvider::isFormatSwitchPending() const {
    return mPendingFormatSwitch.load();
}

bool AudioProvider::switchFormatSkip() {
    // uninit current encoder
    ma_decoder_uninit(&mDecoder);

    // set the new formatting
    mSampleRate = mNextDecoder.outputSampleRate;
    mFormat = mNextDecoder.outputFormat;
    mChannels = mNextDecoder.outputChannels;

    // set the current encoder
    mDecoder = std::move(mNextDecoder);
    mCurrentTrackFramesElapsed.store(0);
    mCurrentTrackLength.store(ma_decoder_get_length_in_pcm_frames(&mDecoder));

    logger_->debug("Switching Format");
    return true;
}

bool AudioProvider::setCurrentTrack(const std::string &uri) {
    mCurrentTrackURI = uri;
    std::string path = mAppContext.getLibrary().getTrackPath(uri);

    ma_result res = ma_decoder_init_file(path.c_str(), NULL, &mNextDecoder);
    if (res != MA_SUCCESS) {
        logger_->error("Failed to load decoder for {}", uri);
        stop();
        return false;
    }

    if (mNextDecoder.outputSampleRate != mSampleRate ||
        mNextDecoder.outputChannels != mChannels ||
        mNextDecoder.outputFormat != mFormat)
    {
        // logic depending whether or not playback is currently happening
        mPendingFormatSwitch.store(true);
        if (mIsPlaying.load()) {
            formatChangeSignal();
        } else {
            switchFormatSkip();
        }
    } else {
        ma_decoder_uninit(&mDecoder);
        mDecoder = std::move(mNextDecoder);
    }


    mCurrentTrackFramesElapsed.store(0);
    mCurrentTrackLength.store(ma_decoder_get_length_in_pcm_frames(&mDecoder));
    logger_->info("Loaded {}", uri);
    return true;
}

bool AudioProvider::setNextTrack(const std::string &path) {
    return false;
}

bool AudioProvider::skip() {
    return false;
}

bool AudioProvider::play() {
    logger_->info("Starting Playback");
    if (mIsPlaying.load()) {
        mIsPaused.store(false);
        return false;
    }

    std::string cur_track = mPlayQueue.currentTrack();
    if (cur_track == "") return false;

    bool res = setCurrentTrack(cur_track);

    if (res) {
        mIsPlaying.store(true);
        mIsPaused.store(false);
        startSignal();
        return res;
    }
    return true;
}

bool AudioProvider::pause() {
    mIsPaused.store(true);
    return false;
}

bool AudioProvider::stop() {
    logger_->info("Stopping Playback");
    mIsPlaying.store(false);
    stopSignal();
    return false;
}

bool AudioProvider::togglePause() {
    if (!mIsPlaying.load()) {
        play();
    } else {
        mIsPaused.store(!mIsPaused.load());
    }
    return !mIsPaused.load();
}

ma_format AudioProvider::getFormat() const {
    return mFormat;
}

uint32_t AudioProvider::getChannels() const {
    return mChannels;
}

uint32_t AudioProvider::getSampleRate() const {
    return mSampleRate;
}

void AudioProvider::mTimeSubscription() {
    unsigned elapsed_time = mCurrentTrackFramesElapsed.load() / ((long)mSampleRate / 1000l);
    json j = {
            {"message-type","update"},
            {"update-type","playback-time"},
            {"data",{
                            {"time", elapsed_time}
            }}
    };

    mSubManager.sendUpdate("playback-time", j);
}


