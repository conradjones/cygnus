//
// Created by user on 6/8/19.
//

#include <iostream>
#include <miniaudio.h>

#include "output.hpp"

#include "miniaudio.h"

void Output::dataCallback(ma_device *device, void *output, const void *input, ma_uint32 frameCount) {
    Output *source = (Output*)device->pUserData;
    if (source == NULL) {
        return;
    }

    source->mProvider.loadFramesToBuffer(frameCount, output);

    (void)input;
}

bool Output::start() {
    mDeviceConfig = ma_device_config_init(ma_device_type_playback);
    mDeviceConfig.playback.format   = mProvider.getFormat();
    mDeviceConfig.playback.channels = mProvider.getChannels();
    mDeviceConfig.sampleRate        = mProvider.getSampleRate();
    mDeviceConfig.dataCallback      = Output::dataCallback;
    mDeviceConfig.pUserData         = this;
    mPaused = false;

    if (ma_device_init(NULL, &mDeviceConfig, &mDevice) != MA_SUCCESS) {
        printf("Failed to open playback device.\n");
        stop();
        //ma_decoder_uninit(&mDecoder);
        return false;
    }

    mFramesElapsed = 0;
    if (ma_device_start(&mDevice) != MA_SUCCESS) {
        printf("Failed to start playback device.\n");
        stop();
        return false;
    }

    mPlaying = true;
    mSampleRate = mDecoder.outputSampleRate;
    mTrackLength.store(ma_decoder_get_length_in_pcm_frames(&mDecoder));
    //mPool.queueFn(std::bind(&Output::mTrackChangeSubscription, this));
    return 0;
}

void Output::stop() {
    ma_device_uninit(&mDevice);
    //ma_decoder_uninit(&mDecoder);

    mPlaying = false;
    mPaused = true;
    mFramesElapsed = 0;
    mSkip = false;
}

void Output::togglePause() {
    mPaused = !mPaused;

    // play if stopped
    if (!mPaused && !mPlaying) {
        //playback();
    }
}

void Output::queueTrack(const std::string &trackName) {
    mPlayQueue.addTrack(trackName);
    // mTrackQueue.push(trackName);
}

void Output::skip() {
    mSkip.store(true);
}

Output::Output(ThreadPool& pool, PlayQueue& queue, SubscriptionManager &subManager, AudioProvider &provider) :
mSkip(false),
mPool(pool),
mPlayQueue(queue),
mSubManager(subManager),
mPaused(true),
mPlaying(false),
mProvider(provider)
{
    mProvider.startSignal.connect(boost::bind(&Output::start, this));
    mProvider.stopSignal.connect(boost::bind(&Output::stop, this));
    mProvider.formatChangeSignal.connect(boost::bind(&Output::formatChange, this));
    //mPlayQueue.stopSignal.connect(boost::bind(&Output::stop, this));
    //mPlayQueue.trackChangedSignal.connect(boost::bind(&Output::skip, this));
}

bool Output::formatChange() {
    stop();
    mProvider.switchFormatSkip();
    return start();
}

