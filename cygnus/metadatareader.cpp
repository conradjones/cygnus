//
// Created by user on 12/8/19.
//

#include "metadatareader.hpp"
#include "stringtools.hpp"

#include <taglib/fileref.h>
#include <boost/filesystem.hpp>
#include <fmt/format.h>

TrackMetadata getFileMetadata(const std::string &path) {
    TrackMetadata meta;
    TagLib::FileRef fr(path.c_str());
    meta.uri = path;
    meta.filename = boost::filesystem::path(path).filename().string();
    if (fr.isNull()) {
        throw std::runtime_error(fmt::format("TagLib: error reading {}", path));
    }
    if (fr.tag()->artist() != TagLib::String::null)
        meta.artist = fr.tag()->artist().toCString(true);
    if (fr.tag()->album() != TagLib::String::null)
        meta.album = fr.tag()->album().toCString(true);
    if (fr.tag()->title() != TagLib::String::null)
        meta.title = fr.tag()->title().toCString(true);

    meta.track = fr.tag()->track();
    meta.length = fr.audioProperties()->lengthInMilliseconds();
    meta.year = fr.tag()->year();
    return std::move(meta);
}

