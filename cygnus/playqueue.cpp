//
// Created by user on 6/18/19.
//

#include <iostream>
#include "playqueue.hpp"
#include "subscriptionmanager.hpp"
#include "metadatareader.hpp"
#include "doctools.hpp"
#include "metadatatools.hpp"

PlayQueue::PlayQueue(SubscriptionManager &subManager, Library &mLibrary) :
        mIndex(-1),
        mSubManager(subManager),
        mLibrary(mLibrary){

    trackChangedSignal.connect(boost::bind(&PlayQueue::mTrackChangedSubscription, this));
    logger_ = spdlog::get("cygnus");

}
void PlayQueue::addTrack(const std::string &trackName) {
    mTracks.push_back(trackName);
    logger_->info("queueing track: {}", trackName);
}


std::string PlayQueue::skipTo(int index) {
    if (index < 0 || index >= mTracks.size()) {
        mIndex = -1;
    } else {
        mIndex = index;
    }
    trackChangedSignal(currentTrack());

    logger_->info("skipping to track: {}", index);
    return currentTrack();
}

std::string PlayQueue::skipRelative(int rel_index) {
    return skipTo(mIndex + rel_index);
}


bool PlayQueue::empty() {
    return mTracks.empty();
}

bool PlayQueue::hasNext() {
    return mIndex + 1 < mTracks.size();
}

std::string PlayQueue::currentTrack() {
    if (mIndex < 0 || mIndex >= mTracks.size()) {
        return "";
    }
    return mTracks[mIndex];
}

void PlayQueue::updateSubscriptions() {
    /*
    json j = {{"message-type", "update"},
              {"update-type", "queue"},
              {"data", getListing()}};
    mSubManager.sendUpdate("queue", j);
     */
}

json PlayQueue::getListing() {
    json j = {{"index", currentIndex()},
              {"entries", {}}};

    for (auto &i : mTracks) {
        j["entries"].push_back(mLibrary.getTrackMetadata(i));
    }
    return j;
}


bool PlayQueue::removeTrack(int index) {
    if (index < 0 || index >= mTracks.size()) {
        return false;
    }

    if (index == mIndex) {
        stopSignal();
        mIndex = -1;
    }

    if (index < mIndex) {
        --mIndex;
    }

    mTracks.erase(mTracks.begin() + index);

    json j = {{"message-type","update"},
              {"update-type","queue"},
              {"data",{
                                      {"mode","remove"},
                                      {"index", index},
                                      {"count",1}
                              }}};

    mSubManager.sendUpdate("queue",j);

    logger_->info("removing track: {}", index);
    return true;
}

int PlayQueue::clear() {
    if (mIndex >= 0) {
        stopSignal();
        mIndex = -1;
    }
    int ret = mTracks.size();
    mTracks.clear();

    json j = {{"message-type","update"},
              {"update-type","queue"},
              {"data",{
                                      {"mode","clear"}
                              }}};

    mSubManager.sendUpdate("queue",j);
    logger_->info("clearing tracks");
    return ret;
}

int PlayQueue::currentIndex() const {
    return mIndex;
}

void PlayQueue::mTrackChangedSubscription() {
    json j = {
            {"message-type", "update"},
            {"update-type", "track-change"},
            {"data",getFileMetadata(mLibrary.getTrackPath(currentTrack()))}
    };
    j["data"]["index"] = currentIndex();

    mSubManager.sendUpdate("track-change", j);


}

int PlayQueue::queueTracks(const std::string &source, const std::string &path) {
    auto tracks = mLibrary.getTracksInPath(source, path);
    json j = {{"message-type","update"},
              {"update-type","queue"},
              {"data",{
                                      {"mode","append"},
                                      {"entries",{}}
                              }}};
    for (auto &i : tracks) {
        addTrack(i);
        j["data"]["entries"].push_back(mLibrary.getTrackMetadata(i));
        logger_->info("queueing track: {}", i);
    }
    mSubManager.sendUpdate("queue", j);
    updateSubscriptions();
    return tracks.size();
}

json PlayQueue::enqueue(const json &args) {
    json j = {{"message-type","update"},
              {"update-type","queue"},
              {"data",{
                                      {"mode","append"},
                                      {"entries",{}}
                              }}};

    auto artist = args.find("artist");
    auto album_id = args.find("album-id");
    auto album = args.find("album");
    auto uris = args.find("uris");

    std::vector<std::string> tracks_to_add;
    if (artist != args.end()) {
        if (album != args.end()) {
            tracks_to_add = mLibrary.getTracksInAlbum(artist.value(), album.value());
        } else {
            tracks_to_add = mLibrary.getTracksByArtist(artist.value());
        }
    } else if (album_id != args.end()) {
        tracks_to_add = mLibrary.getTracksInAlbum(album_id.value());
    } else if (uris != args.end()) {
        for (auto &i : uris.value()) {
            tracks_to_add.push_back(i);
        }
    }

    int index = mTracks.size();
    int added = tracks_to_add.size();
    for (auto &i : tracks_to_add) {
        addTrack(i);
        j["data"]["entries"].push_back(mLibrary.getTrackMetadata(i));
    }

    mSubManager.sendUpdate("queue", j);
    return j["data"];
}

