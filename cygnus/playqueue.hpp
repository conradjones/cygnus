//
// Created by user on 6/18/19.
//

#ifndef CYGNUS_PLAYQUEUE_HPP
#define CYGNUS_PLAYQUEUE_HPP

#include <vector>
#include <string>
#include "clientcontext.hpp"
#include "library.hpp"

#include <boost/signals2/signal.hpp>
#include <spdlog/spdlog.h>

class SubscriptionManager;

class PlayQueue {
public:
    PlayQueue(SubscriptionManager &subManager, Library &mLibrary);

    void addTrack(const std::string& trackName);
    bool hasNext();
    std::string skipTo(int index);
    std::string skipRelative(int rel_index);
    bool empty();
    std::string currentTrack();
    int currentIndex() const;

    bool removeTrack(int index);

    int clear();

    json getListing();

    void updateSubscriptions();

    int queueTracks(const std::string &source, const std::string &path);
    json enqueue(const json &args);

    // signals
    boost::signals2::signal<void()> stopSignal;
    boost::signals2::signal<void(const std::string &uri)> trackChangedSignal;

protected:
    void mTrackChangedSubscription();

    std::vector<std::string> mTracks;
    int mIndex;
    SubscriptionManager &mSubManager;
    Library &mLibrary;
    std::shared_ptr<spdlog::logger> logger_;
};


#endif //CYGNUS_PLAYQUEUE_HPP
