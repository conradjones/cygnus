//
// Created by user on 12/19/19.
//

#ifndef CYGNUS_APPCONTEXT_HPP
#define CYGNUS_APPCONTEXT_HPP


#include "threadpool.hpp"
#include "library.hpp"
#include "subscriptionmanager.hpp"
#include "playqueue.hpp"
#include "audioprovider.hpp"
#include "output.hpp"

class AppContext {
public:
    AppContext(ThreadPool &pool);

    ThreadPool &getThreadPool();
    Library &getLibrary();
    SubscriptionManager &getSubscriptionManager();
    PlayQueue &getPlayQueue();
    AudioProvider &getAudioProvider();
    Output &getOutput();
private:
    ThreadPool &mThreadPool;
    Library mLibrary;
    SubscriptionManager mSubscriptionManager;
    PlayQueue mPlayQueue;
    AudioProvider mAudioProvider;
    Output mOutput;
};


#endif //CYGNUS_APPCONTEXT_HPP
