#include <iostream>
#include <fmt/format.h>


#include "output.hpp"
#include "threadpool.hpp"
#include "library.hpp"
#include "subscriptionmanager.hpp"
#include "connectionmanager.hpp"
#include "messagedispatcher.hpp"
#include "config.hpp"
#include "fstools.hpp"
#include "actionwrapper.hpp"
#include "appcontext.hpp"

#include <spdlog/spdlog.h>
#include <spdlog/async.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/dist_sink.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <toml11/toml.hpp>

int main() {
    std::shared_ptr<spdlog::logger> logger = nullptr;
    try {
        auto dist_sink = std::make_shared<spdlog::sinks::dist_sink_mt>();
        auto sink1 = std::make_shared<spdlog::sinks::stderr_color_sink_mt>();
        auto sink2 = std::make_shared<spdlog::sinks::rotating_file_sink_mt>("cygnus.log", 1048576 * 5, 3);

        dist_sink->add_sink(sink1);
        dist_sink->add_sink(sink2);

        spdlog::init_thread_pool(8192, 1);
        logger = std::make_shared<spdlog::async_logger>("cygnus", dist_sink, spdlog::thread_pool(), spdlog::async_overflow_policy::block);
        spdlog::register_logger(logger);
        logger->set_level(spdlog::level::debug);

        logger->info("Initialized Logger");
    } catch (const std::exception& e) {
        std::cout << "EXCEPTION: " << e.what();
    }
    // load config
    /*
    std::vector<std::string> paths = {"./cygnus.ini",
                                      fmt::format("{}/.config/cygnus/cygnus.ini", fstools::env("HOME"))};
    Config conf(fstools::fs_fallback(paths));
     */
    std::vector<std::string> paths = {"./cygnus.toml",
                                      fmt::format("{}/.config/cygnus/cygnus.toml", fstools::env("HOME"))};
    const auto conf = toml::parse(fstools::fs_fallback(paths));

    ThreadPool tp;
    tp.spawnWorkers(4);

    AppContext ac(tp);

    Library &l = ac.getLibrary();

    // get library sources
    const auto &conf_libs = toml::find<std::vector<toml::table>>(conf, "libraries");

    for (const auto &i : conf_libs) {
        l.addSource(i.at("name").as_string(), std::string(i.at("path").as_string()));
    }

    // set up connection manager
    ConnectionManager cm;

    // set up subscription manager
    SubscriptionManager &subManager = ac.getSubscriptionManager();

    // set up play queue
    PlayQueue &queue = ac.getPlayQueue();

    // set up audio provider
    AudioProvider &provider = ac.getAudioProvider();

    // set up output
    Output &out = ac.getOutput();

    // do something when a client connects
    auto newConFn = [&logger](std::shared_ptr<ClientContext> ctx) {
        logger->info("New client connected ({}:{})", ctx->getAddress(), ctx->getPort());
    };


    //ping function
    auto pingFn = [](std::shared_ptr<ClientContext> ctx, uint8_t tag, const json &doc) {
        // create response
        /*
        rapidjson::Document res;
        res.SetObject();
        res.AddMember("response", "pong", res.GetAllocator());

        // send response
        ctx->send(res, tag);
         */
    };

    // set up message dispatcher
    MessageDispatcher dispatcher(tp);

    // signals
    cm.newConnectionSignal.connect(newConFn);

    cm.processMessageSignal.connect(boost::bind(&MessageDispatcher::processMsg, &dispatcher,_1,_2,_3));

    // SET UP ACTIONS

    // skip to action
    ActionWrapper<std::string, int> skip_to_wrapper(
            std::bind(&PlayQueue::skipTo, &queue, std::placeholders::_1),
            "queue-skip-to",
            {"index"},
            std::tuple(std::nullopt));
    dispatcher.getSignal("skip-to").connect(skip_to_wrapper);

    // list dir action
    ActionWrapper<json, std::string, std::string> list_dir_wrapper(
            std::bind(&Library::getListingJSON,&l,std::placeholders::_1,std::placeholders::_2),
            "list-dir",
            {"source","path"},
            std::tuple(std::nullopt, std::string("/")));
    dispatcher.getSignal("list-dir").connect(list_dir_wrapper);

    // toggle pause action
    ActionWrapperNoArgs<bool> toggle_pause_wrapper(
            std::bind(&AudioProvider::togglePause, &provider),
            "toggle-pause");
    dispatcher.getSignal("toggle-pause").connect(toggle_pause_wrapper);

    // subscribe action
    dispatcher.getSignal("subscribe"   ).connect(boost::bind(&SubscriptionManager::subscribeAction, &subManager, _1, _2, _3));

    // skip action
    ActionWrapper<std::string, int> skip_relative_wrapper(
            std::bind(&PlayQueue::skipRelative, &queue, std::placeholders::_1),
            "skip",
            {"index"},
            std::tuple(1)
            );
    dispatcher.getSignal("skip").connect(skip_relative_wrapper);

    // queue tracks action
    ActionWrapper<int, std::string, std::string> queue_tracks_wrapper(
            std::bind(&PlayQueue::queueTracks, &queue, std::placeholders::_1, std::placeholders::_2),
            "queue-tracks",
            {"source", "path"},
            std::tuple(std::nullopt, std::string("/"))
            );
    dispatcher.getSignal("queue-tracks").connect(queue_tracks_wrapper);

    // queue clear
    ActionWrapperNoArgs<int> queue_clear_wrapper(
            std::bind(&PlayQueue::clear, &queue),
            "queue-clear"
            );
    dispatcher.getSignal("queue-clear").connect(queue_clear_wrapper);

    // queue remove track
    ActionWrapper<bool, int> queue_remove_wrapper(
            std::bind(&PlayQueue::removeTrack, &queue, std::placeholders::_1),
            "queue-remove",
            {"index"},
            std::tuple(std::nullopt)
            );
    dispatcher.getSignal("queue-remove").connect(queue_remove_wrapper);

    // queue get listing
    ActionWrapperNoArgs<json> queue_get_listing_wrapper(
            std::bind(&PlayQueue::getListing, &queue),
            "queue-list"
            );
    dispatcher.getSignal("queue-list").connect(queue_get_listing_wrapper);

    // library scan action
    ActionWrapper<int, std::string, std::string> library_scan_wrapper(
            std::bind(&Library::scan, &l, std::placeholders::_1, std::placeholders::_2),
            "library-scan",
            {"source", "path"},
            std::tuple(std::nullopt, std::nullopt)
            );
    dispatcher.getSignal("library-scan").connect(library_scan_wrapper);

    // library search action
    ActionWrapper<json, std::string, std::string, int> library_search_wrapper(
            std::bind(&Library::search, &l, std::placeholders::_1, std::placeholders::_2,std::placeholders::_3),
            "library-search",
            {"mode", "query", "limit"},
            std::tuple(std::string("all"),std::nullopt,100));
    dispatcher.getSignal("library-search").connect(library_search_wrapper);

    ActionWrapperNoArgs<std::vector<std::string>> library_list_sources(
            std::bind(&Library::getSources, &l), "library-list-sources"
            );
    dispatcher.getSignal("library-list-sources").connect(library_list_sources);

    // enqueue action
    ActionWrapperJSONArg<json> queue_enqueue_wrapper(
            std::bind(&PlayQueue::enqueue, &queue, std::placeholders::_1),
            "enqueue"
            );
    dispatcher.getSignal("enqueue").connect(queue_enqueue_wrapper);
    /*
    dispatcher.getSignal("ping"        ).connect(pingFn);
    dispatcher.getSignal("list-dir"    ).connect(boost::bind(&Library::getListingAction, &l, _1, _2, _3));
    dispatcher.getSignal("subscribe"   ).connect(boost::bind(&SubscriptionManager::subscribeAction, &subManager, _1, _2, _3));
    dispatcher.getSignal("toggle-pause").connect(boost::bind(&AudioProvider::togglePause, &provider));
    dispatcher.getSignal("skip"        ).connect(boost::bind(&PlayQueue::skipRelative, &queue, 1));
    //dispatcher.getSignal("skip-to"     ).connect(boost::bind(&PlayQueue::skipToAction, &queue, _1, _2, _3));
    dispatcher.getSignal("queue-add"   ).connect(boost::bind(&PlayQueue::queueTrackAction, &queue, _1, _2, _3));
    dispatcher.getSignal("queue-list"  ).connect(boost::bind(&PlayQueue::getListingAction, &queue, _1, _2, _3));
    dispatcher.getSignal("queue-remove").connect(boost::bind(&PlayQueue::removeAction, &queue, _1, _2, _3));
    dispatcher.getSignal("queue-clear" ).connect(boost::bind(&PlayQueue::clear, &queue));
    dispatcher.getSignal("queue-trackinfo").connect(boost::bind(&PlayQueue::trackInfoAction, &queue, _1, _2, _3));
     */

    cm.listenOnPort(conf.at("connection").at("port").as_integer());

    while(true) {
        cm.process();
    }

    return 0;
}