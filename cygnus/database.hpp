//
// Created by user on 12/21/19.
//

#ifndef CYGNUS_DATABASE_HPP
#define CYGNUS_DATABASE_HPP

#include <sqlite3.h>
#include <string>
#include "dbcursor.hpp"

class Database {
public:
    Database(const std::string &file_name);
    ~Database();

    DBCursor exec(const std::string &query);

    template <typename T>
    DBCursor exec(const std::string &query, const T &args) {
        auto stmt = prepare(query);
        mBindStatement(stmt, args);
        return DBCursor(stmt);
    }

    /// returns true if the DB did not exist before now
    bool isFresh() const;

private:
    sqlite3_stmt *prepare(const std::string &query);

    template<typename T, size_t I = 0>
    void mBindStatement(sqlite3_stmt *stmt, const T& args) {
        if constexpr (I == std::tuple_size<T>()) {
            return;
        } else {
            int rc = bind(stmt, I + 1, std::get<I>(args));
            if (rc != SQLITE_OK) {
                throw std::runtime_error("SQL failed to bind parameter");
            } else {
                mBindStatement<T, I+1>(stmt, args);
            }
        }
    }

    int bind(sqlite3_stmt *stmt, int i, int arg) {
        return sqlite3_bind_int(stmt, i, arg);
    }

    int bind(sqlite3_stmt *stmt, int i, const std::string& arg) {
        return sqlite3_bind_text(stmt, i, &arg[0], arg.size(), SQLITE_TRANSIENT);
    }

    sqlite3 *mDB;
    bool mIsFresh;
};

#endif //CYGNUS_DATABASE_HPP
