//
// Created by user on 12/21/19.
//

#include <stdexcept>
#include <fmt/format.h>
#include <boost/filesystem.hpp>

#include "database.hpp"

Database::Database(const std::string &file_name) {
    mIsFresh = !boost::filesystem::exists(file_name);

    int rc = sqlite3_open(file_name.c_str(), &mDB);
    if (rc) {
        throw std::runtime_error(fmt::format("SQLite failed to open database ({})", file_name));
    }
}

Database::~Database() {
    sqlite3_close(mDB);
}

DBCursor Database::exec(const std::string &query) {
    return DBCursor(prepare(query));
}

sqlite3_stmt *Database::prepare(const std::string &query) {
    char *zErrorMsg = 0;
    sqlite3_stmt *statement = nullptr;

    int rc = sqlite3_prepare_v2(mDB, query.c_str(), query.size(), &statement, nullptr);
    if (rc != SQLITE_OK) {
        throw std::runtime_error ("SQLite prepare error");
    }

    return statement;
}

bool Database::isFresh() const {
    return mIsFresh;
}


