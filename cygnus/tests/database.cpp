//
// Created by user on 12/21/19.
//

#include <boost/filesystem.hpp>
#include "catch.hpp"

#include "../database.hpp"

TEST_CASE( "Databse creation and execution", "[Database]") {
    Database db("test.db");

    std::string track_table =
            "CREATE TABLE track(" \
            "id INT PRIMARY KEY NOT NULL," \
            "uri TEXT NOT NULL," \
            "track INT," \
            "title TEXT, " \
            "artist INT, " \
            "album INT);";

    {
        auto cur = db.exec(track_table);
        REQUIRE(cur.isDone() == true);
    }

    std::string insert_tracks =
            "INSERT INTO track (id,uri) " \
            "VALUES (?,?) ";

    {
        auto cur = db.exec(insert_tracks, std::tuple(1, "music:/song.flac"));
        REQUIRE(cur.isDone() == true);
    }

    std::string get_tracks =
            "SELECT * FROM track";
    {
        auto cur = db.exec(get_tracks);
        REQUIRE(cur.columnCount() == 6);
        REQUIRE(std::string(cur.columnName(0)) == "id");
        REQUIRE(cur.getInt(0) == 1);
        REQUIRE(cur.getString(1) == "music:/song.flac");

        cur.step();
        REQUIRE(cur.isDone() == true);
    }

    // remove db
    boost::filesystem::remove("test.db");
}
