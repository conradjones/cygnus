//
// Created by user on 6/14/19.
//

#include <vector>
#include <algorithm>
#include <fmt/format.h>
#include <spdlog/spdlog.h>

#include "library.hpp"
#include "metadatareader.hpp"

Library::Library() :
    mDB("library.db")
{
    if (mDB.isFresh()) mSetupSchema();
}

bool Library::addSource(const std::string &name, const boost::filesystem::path& path) {
    if (boost::filesystem::exists(path)) {
        mSources[name] = path;
        return true;
    }
    return false;
}

std::string Library::getTrackPath(const std::string &library, const std::string &path) {
    auto& libPath = mSources[library];
    auto joinedPath = boost::filesystem::path(libPath) / boost::filesystem::path(path.substr(1,std::string::npos));
    return std::string(joinedPath.string());
}

std::vector<std::string> Library::getTracksInPath(const std::string &source, const std::string &path) {
    boost::filesystem::path std_path = mSources[source] / boost::filesystem::path(path.substr(1, std::string::npos));
    if (!boost::filesystem::exists(std_path)) return {};
    if (!boost::filesystem::is_directory(std_path)) {
        return {fmt::format("{}:{}", source, path)};
    }

    std::vector<std::string> track_list;
    std::vector<std::string> dir_list;

    for (auto& p : boost::filesystem::directory_iterator(std_path)) {
        // get filename
        auto str = p.path().string().substr(mSources[source].string().length(), std::string::npos);
        std::string type = "file";
        if (boost::filesystem::is_directory(p)) {
            auto child_tracks = getTracksInPath(source, str);
            track_list.insert(track_list.end(), child_tracks.begin(), child_tracks.end());
        } else if (
                (p.path().extension() == ".flac") ||
                (p.path().extension() == ".mp3") ||
                (p.path().extension() == ".ogg")
        ) {
            // cut out library path
            std::string uri = fmt::format("{}:{}",
                    source,
                    p.path().string().substr(mSources[source].string().size(), std::string::npos));
            track_list.push_back(uri);
        }
    }

    std::sort(track_list.begin(), track_list.end());
    return track_list;
}


json Library::getListingJSON(const std::string &source, const std::string &path) {
    json j = {{"source", source},
              {"path", path},
              {"entries", {}}};

    std::vector<std::pair<std::string, std::string>> itemList;
    auto scanPath = mSources[source] / boost::filesystem::path(path.substr(1, std::string::npos));
    for (auto& p : boost::filesystem::directory_iterator(scanPath)) {
        // get filename
        auto str = p.path().string().substr(mSources[source].string().length(), std::string::npos);
        std::string type = "file";
        if (boost::filesystem::is_directory(p)) {
            type = "dir";
        }

        itemList.push_back(std::make_pair(str, type));
    }
    // alphabetize list
    std::sort(itemList.begin(), itemList.end());

    for (auto &i : itemList) {
        j["entries"].push_back({{"name", i.first},{"type",i.second}});
    }

    return j;
}

std::string Library::getTrackPath(const std::string &uri) {
    // split uri
    size_t split_loc = uri.find_first_of(':');
    std::string source = uri.substr(0, split_loc);
    std::string path = uri.substr(split_loc + 1, std::string::npos);

    return mSources[source].string() + path;
}

TrackMetadata Library::getTrackMetadata(const std::string &uri) {
    return getFileMetadata(getTrackPath(uri));
}

void Library::mSetupSchema() {
    // set up track table
    std::string track_table =
            "CREATE TABLE track(" \
            "track_id INTEGER PRIMARY KEY," \
            "uri TEXT NOT NULL," \
            "filename TEXT NOT NULL," \
            "track INT," \
            "title TEXT, " \
            "artist_id INT, " \
            "album_id INT);";
    mDB.exec(track_table);

    // set up album table
    std::string album_table =
            "CREATE TABLE album(" \
            "album_id INTEGER PRIMARY KEY," \
            "title TEXT NOT NULL," \
            "year INT);";
    mDB.exec(album_table);

    // set up artist table
    std::string artist_table =
            "CREATE TABLE artist(" \
            "artist_id INTEGER PRIMARY KEY," \
            "name TEXT NOT NULL);";
    mDB.exec(artist_table);

    std::string album_artists_table =
            "CREATE TABLE album_artist(" \
            "artist_id INTEGER NOT NULL, " \
            "album_id INTEGER NOT NULL, " \
            "PRIMARY KEY(artist_id,album_id));";
    mDB.exec(album_artists_table);
}

int Library::scan(const std::string &source, const std::string &path) {
    auto logger = spdlog::get("cygnus");
    int count = 0;
    auto tracks = getTracksInPath(source, path);
    logger->info("Scan started on {}:{}", source, path);
    for (auto &i : tracks) {
        // check if db already contains the uri
        if (mDB.exec("SELECT uri FROM track WHERE uri=?", std::tuple(i)).isRow())
            continue;

        // get file metadata
        try {
            auto meta = getTrackMetadata(i);
            int artist_id = 0;
            int album_id = 0;

            if (meta.artist != "") {
                // get artist id
                auto cur = mDB.exec("SELECT artist_id FROM artist WHERE name=?", std::tuple(meta.artist));
                if (cur.isRow()) {
                    //std::cout << "artist exists" << std::endl;
                    artist_id = cur.getInt(0);
                } else {
                    // add the artist if it doesn't exist
                    mDB.exec("INSERT INTO artist (name) VALUES (?)", std::tuple(meta.artist));
                    auto cur2 = mDB.exec("SELECT artist_id FROM artist WHERE name=?", std::tuple(meta.artist));
                    artist_id = cur2.getInt(0);
                }
            }

            // get album id
            if (meta.album != "") {
                std::string sql =
                        "SELECT album_id " \
                        "FROM album " \
                        "WHERE title=? " \
                        "AND year=? ";
                auto cur = mDB.exec(sql, std::tuple(meta.album, meta.year));
                if (cur.isRow()) {
                    album_id = cur.getInt(0);
                } else {
                    mDB.exec("INSERT INTO album (title, year) VALUES (?,?)", std::tuple(meta.album, meta.year));
                    auto cur2 = mDB.exec("SELECT album_id FROM album WHERE title=?", std::tuple(meta.album));
                    album_id = cur2.getInt(0);
                }
            }

            // add album/artist link
            if (meta.artist !=  "" && meta.album != "") {
                auto cur = mDB.exec(
                        "SELECT * FROM album_artist WHERE album_id=? AND artist_id=?",
                        std::tuple(album_id, artist_id));
                if (!cur.isRow()) {
                    mDB.exec(
                            "INSERT INTO album_artist (album_id, artist_id) VALUES (?,?)",
                            std::tuple(album_id, artist_id));
                }
            }

            std::string query =
                    "INSERT INTO track " \
                    "(uri, filename, track, title, artist_id, album_id) " \
                    "VALUES (?, ?, ?, ?, ?, ?)";
            mDB.exec(query, std::tuple(i, meta.filename, meta.track, meta.title, artist_id, album_id));
            ++count;
        } catch (const std::exception &e) {
            logger->warn(e.what());
        }
    }
    logger->info("Scan complete");
    return count;
}

json Library::search(const std::string &mode, const std::string &query, int limit) {
    json ret = {};
    if (mode == "tracks" || mode == "all") {
        ret["tracks"] = {};
        std::string sql =
                "SELECT track.track_id, artist.name, album.title, track.title, track.uri " \
                "FROM track " \
                "JOIN artist ON track.artist_id=artist.artist_id " \
                "JOIN album ON track.album_id=album.album_id " \
                "WHERE track.title LIKE ? " \
                "LIMIT ? ";
        auto cur = mDB.exec(sql, std::tuple(query, limit));
        while (cur.isRow()) {
            json track = {
                    {"id", cur.getInt(0)},
                    {"artist", cur.getString(1)},
                    {"album", cur.getString(2)},
                    {"title", cur.getString(3)},
                    {"uri", cur.getString(4)}
            };
            ret["tracks"].push_back(track);
            cur.step();
        }
    }
    if (mode == "albums" || mode == "all") {
        ret["albums"] = {};
        std::string sql =
                "SELECT album.album_id, album.title " \
                "FROM album " \
                "WHERE album.title LIKE ? " \
                "LIMIT ? ";
        auto cur = mDB.exec(sql, std::tuple(query, limit));
        while (cur.isRow()) {
            json album = {
                    {"id", cur.getInt(0)},
                    {"title", cur.getString(1)}
            };
            ret["albums"].push_back(album);
            cur.step();
        }
    }
    if (mode == "artists" || mode == "all") {
        ret["artists"] = {};
        auto cur = mDB.exec("SELECT name FROM artist WHERE name LIKE ? LIMIT ?", std::tuple(query, limit));
        while (cur.isRow()) {
            ret["artists"].push_back(cur.getString(0));
            cur.step();
        }
    }
    return ret;
}

std::vector<std::string> Library::getTracksByArtist(const std::string &name) {
    std::vector<std::string> tracks;
    std::string sql =
            "SELECT track.uri " \
            "FROM track " \
            "JOIN artist ON track.artist_id=artist.artist_id " \
            "JOIN album ON track.album_id=album.album_id " \
            "WHERE artist.name=? " \
            "ORDER BY album.year, track.track, track.filename, track.uri ASC";

    auto cur = mDB.exec(sql, std::tuple(name));
    while (cur.isRow()) {
        tracks.push_back(cur.getString(0));
        cur.step();
    }

    return tracks;
}

std::vector<std::string> Library::getTracksInAlbum(const std::string &artist, const std::string &album) {
    std::vector<std::string> tracks;
    std::string sql =
            "SELECT track.uri " \
            "FROM track " \
            "JOIN album ON track.album_id=album.album_id " \
            "JOIN album_artist ON album.album_id=album_artist.album_id " \
            "JOIN artist ON artist.artist_id=album_artist.artist_id " \
            "WHERE artist.name=? AND album.title=? " \
            "ORDER BY track.track, track.filename, track.uri ASC";

    auto cur = mDB.exec(sql, std::tuple(artist, album));
    while (cur.isRow()) {
        tracks.push_back(cur.getString(0));
        cur.step();
    }

    return tracks;
}

std::vector<std::string> Library::getTracksInAlbum(int album_id) {
    std::vector<std::string> tracks;
    auto cur = mDB.exec("SELECT track.uri FROM track WHERE album_id=?", std::tuple(album_id));
    while (cur.isRow()) {
        tracks.push_back(cur.getString(0));
        cur.step();
    }

    return tracks;
}

std::vector<std::string> Library::getSources() {
    std::vector<std::string> source_list;
    for (const auto & [key, val] : mSources) {
        source_list.push_back(key);
    }
    return source_list;
}

